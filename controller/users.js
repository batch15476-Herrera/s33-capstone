//[SECTION] Dependencies and Modules
const User = require('../models/User'); 
const bcrypt = require('bcrypt');
const dotenv = require('dotenv');

//[SECTION] Environment Variables Setup
dotenv.config();
const asin = Number(process.env.SALT)

module.exports.register = (userData) => {
    let email= userData.email;
    let passW= userData.password;
    
    let newUser = new User({
        email: email,
        password: bcrypt.hashSync(passW, asin),
    });
    return newUser.save().then((user, err) => {
        if (user) {
            return user;
        } else {
            return 'Failed to Register account.';
        };
    });
};


//[SECTION] Functionalities [RETRIEVE]
//[SECTION] Functionalities [UPDATE]
//[SECTION] Functionalities [DELETE]