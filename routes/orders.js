//[SECTION] Dependencies and Modules
const exp = require('express');
const controller = require('../controller/orders');

//[SECTION] Routing Component
const route = exp.Router();

//[SECTION] Routes- POST
route.post('/register', (req,res) => {
    console.log(req.body);
    let userOrder = req.body;
    
    controller.register(userOrder).then(resulta => {
        res.send(resulta)
    });
});

module.exports = route;