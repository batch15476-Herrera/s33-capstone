//[SECTION] Dependencies and Modules
const mongoose = require('mongoose')

//[SECTION] Schema/Blueprint
const orderSchema = new mongoose.Schema({
    totalAmount: {
        type: Number,
        required: [true, "Name is Required."]
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    }
});
//[SECTION] Model
module.exports = mongoose.model('Order', orderSchema);